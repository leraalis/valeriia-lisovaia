'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cleancss = require('gulp-clean-css');
var rename = require("gulp-rename");
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
	return gulp.src('markup/assets/**/*.scss') 
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError)) 
		.pipe(rename({ suffix: ".min" }))
		.pipe(cleancss())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('markup/css/'));
});

gulp.task('watch', function () {
    gulp.watch('markup/assets/**/*.scss', gulp.series(['sass']));
 });

 gulp.task('default', gulp.series(['sass', 'watch']));




